"""
    Fichier : config_app.py
    Auteur : OM 2021.03.15 Lecture des variables d'environnment du fichier ".env"

    Paramètres : Fichier nommé ".env" situé dans le répertoire principal
                Le fichier ".env" DOIT être signalé dans le ".gitignore",
                car il contient des données confidentielles (key, passwords, etc)
"""
import os

import environs
from environs import Env

from APP_FILMS.erreurs.exceptions import ErreurFichierEnvironnement

nom_fichier_variables_env = "APP_FILMS/.env"


def config_application():
    try:
        """
            On accède aux variables d'environnement de l'utilisateur 
        """
        try:
            # Si le fichier n'existe pas. Permet de provoquer une erreur "FileNotFoundError"
            file_size = os.path.getsize(nom_fichier_variables_env)

            obj_env = Env()
            obj_env.read_env()

            HOST_MYSQL = obj_env("HOST_MYSQL")
            USER_MYSQL = obj_env("USER_MYSQL")
            PASS_MYSQL = obj_env("PASS_MYSQL")
            PORT_MYSQL = obj_env("PORT_MYSQL")

            ADRESSE_SRV_FLASK = obj_env.int("ADRESSE_SRV_FLASK")
            DEBUG_FLASK = obj_env("DEBUG_FLASK")
            PORT_FLASK = obj_env("PORT_FLASK")
            SECRET_KEY_FLASK = obj_env("SECRET_KEY_FLASK")

        except FileNotFoundError as erreur_file_not_found:
            print(f"Erreur fichier \".env\" introuvable (nom, emplacement, etc) (variables environnement)",
                  f"{erreur_file_not_found.args[0]}, "
                  f"{erreur_file_not_found}")
            raise

    except environs.EnvError as NameVariableEnv:
        print(f"Problème avec les variables d'environnement "
              f"{NameVariableEnv.args[0]} , "
              f"{NameVariableEnv}")

    except Exception as erreur_fichier_environnement:
        raise ErreurFichierEnvironnement(f"Problème avec le fichier \".env\" (nom, emplacement, etc)")
        # print(f"Problème avec le fichier \".env\" (nom, emplacement, etc) "
        #       f"{ErreurLectureFile.args[0]} , "
        #       f"{ErreurLectureFile}")
    return HOST_MYSQL, USER_MYSQL, PASS_MYSQL, PORT_MYSQL, ADRESSE_SRV_FLASK, DEBUG_FLASK, PORT_FLASK, SECRET_KEY_FLASK

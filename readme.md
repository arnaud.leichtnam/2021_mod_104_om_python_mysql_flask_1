Module 104 "Hurlements en Python et FLASK"
---


## Travail de l’élève
### Constater le l'affichage du contenu de la table "t_genre"

* Démarrer le serveur MySql (uwamp ou xamp ou mamp, etc)
* Récupérer le projet stocké sur Gitlab avec l’IDE PyCharm.
  * Explications sur le MOODLE de l’EPSIC (Module 104).

* Dans PyCharm ouvrir le répertoire "zzzdemos", puis ouvrir le fichier "1_ImportationDumpSql.py".  
  Ensuite avec le bouton de droite de la souris cliquer sur "run" de ce fichier "1_ImportationDumpSql.py".
  * En cas d'erreurs : ouvrir le fichier à la racine du projet (Config_App.yml), contrôler les indications de connexion pour la bd.   

### Pour cette démo

* Il y a un template CSS, stocké en local dans un répertoire (static) IMPOSE par FLASK.
* On réalise UNIQUEMENT (choix de OM de la 707) l'action READ du CRUD (Create Read Update Delete) sur la table "t_genre". 
* Il faut tester le système de gestion des erreurs selon la liste du premier exercice.

### VOTRE travail pour l'exercice 2 :

* Placer votre fichier DUMP au bon endroit comme pour le premier exercice. 
* L'importer grâce au fichier zzzdemos/1_ImportationDumpSql.py
* Adapter les changements pour que UNE seule table de votre projet s'affiche.
* Niveau sup. : Adapter les changements pour que toutes les tables principales de votre projet s'affichent (pas les tables intermédiaires)
* Quelque soit l'état de votre exercice 2. Vous devez le mettre à ma disposition sur Gitlab comme le premier exercice.
